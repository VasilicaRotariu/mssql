CREATE TABLE [dbo].[TestValidFrom]
(
[Description] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ValidFrom] [datetime] NOT NULL CONSTRAINT [DF_TestValidFrom_ValidFrom] DEFAULT (getdate()),
[InvalidFrom] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[TestValidFromAfterInsert] ON [dbo].[TestValidFrom] AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @toDo TABLE ([row] INT NOT NULL IDENTITY(1, 1), [Description] VARCHAR(50), [ValidFrom] DATETIME);
	INSERT INTO @toDo SELECT [Description], [ValidFrom] FROM INSERTED;
	DECLARE @row INT = 1, @rows INT = @@ROWCOUNT;

    DECLARE 
		@Description VARCHAR(50),
		@ValidFrom DATETIME;

	WHILE (@row <= @rows)
	BEGIN
		SELECT
			@Description = [Description],
			@ValidFrom = [ValidFrom]
		FROM @toDo WHERE [row] = @row;
		
		IF EXISTS (SELECT [ValidFrom] FROM [dbo].[TestValidFrom] WHERE [Description] = @Description)
		BEGIN
			UPDATE [dbo].[TestValidFrom]
			SET [InvalidFrom] = @ValidFrom
			WHERE [Description] = @Description
			AND [ValidFrom] < @ValidFrom
			AND [InvalidFrom] IS NULL;
		END
		
		SET @row += 1;
	END
END
GO
