CREATE TABLE [dbo].[DFi21DAFSerialNumbers]
(
[TDate] [datetime2] NOT NULL CONSTRAINT [DF__DFi21DAFS__TDate__0EA330E9] DEFAULT (sysdatetime()),
[SerialNumber] [varchar] (9) COLLATE Latin1_General_CI_AS NOT NULL
) ON [PRIMARY]
GO
