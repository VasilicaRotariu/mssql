CREATE TABLE [dbo].[Destination]
(
[Plant] [smallint] NOT NULL,
[Line] [tinyint] NOT NULL
) ON [PRIMARY]
GO
