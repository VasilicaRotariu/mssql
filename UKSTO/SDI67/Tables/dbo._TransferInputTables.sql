CREATE TABLE [dbo].[_TransferInputTables]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[DateAdded] [datetime] NOT NULL CONSTRAINT [DF__TransferInputTables_DateAdded] DEFAULT (getdate()),
[UpdateBehaviour] [int] NOT NULL,
[SourceTableName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[DestinationTableName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Columns] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[SourceConnectionString] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Priority] [int] NOT NULL CONSTRAINT [DF__TransferInputTables_Priority] DEFAULT ((1)),
[InUse] [tinyint] NOT NULL CONSTRAINT [DF__TransferInputTables_InUse] DEFAULT ((1)),
[RequiresKeyField] [int] NOT NULL CONSTRAINT [DF__TransferInputTables_RequiresKeyfield] DEFAULT ((1)),
[KeyField] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF__TransferInputTables_KeyField] DEFAULT ('PlantNumber'),
[KeyValue] [varchar] (100) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[_TransferInputTables] ADD CONSTRAINT [PK__TransferInputTables] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
