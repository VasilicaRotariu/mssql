CREATE TABLE [dbo].[TestIdentity]
(
[UID] [int] NOT NULL IDENTITY(1, 1),
[Value] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[FileBinary] [varbinary] (max) NULL,
[ParentUID] [int] NULL
) ON [PRIMARY]
GO
