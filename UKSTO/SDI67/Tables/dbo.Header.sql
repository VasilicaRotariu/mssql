CREATE TABLE [dbo].[Header]
(
[LineNumber] [tinyint] NOT NULL,
[UTID] [int] NOT NULL IDENTITY(-2147483648, 1),
[ISN] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[TDate] [datetime2] NOT NULL CONSTRAINT [DF__Header__TDate__08EA5793] DEFAULT (sysdatetime())
) ON [PRIMARY]
GO
