CREATE TABLE [dbo].[CMaticRaw]
(
[Hostname] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PDate] [datetime] NULL,
[ProcessType] [int] NULL,
[RawString] [varchar] (2048) COLLATE Latin1_General_CI_AS NULL,
[Retest] [int] NULL,
[Status] [int] NULL
) ON [PRIMARY]
GO
