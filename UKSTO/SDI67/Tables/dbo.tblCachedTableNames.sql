CREATE TABLE [dbo].[tblCachedTableNames]
(
[TableName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Output] [int] NOT NULL,
[MachineDate] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[UnitIdentifier] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Server] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DatabaseName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[UniqueRowColumns] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[TotalRowsCopied] [int] NULL,
[RowsCopiedLastScan] [int] NULL,
[LastUpdated] [datetime] NULL,
[StatusMessage] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PlantNumberField] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LocalTableName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
) ON [PRIMARY]
GO
