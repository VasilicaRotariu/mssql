CREATE TABLE [dbo].[_TransferTables]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[DateAdded] [datetime] NOT NULL CONSTRAINT [DF__TransferTables_DateAdded] DEFAULT (getdate()),
[LocalTableName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[DestinationTableName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[KeyColumn] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Columns] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[ConnectionStringDestination] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Priority] [int] NULL,
[InUse] [tinyint] NULL,
[Retain] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[_TransferTables] ADD CONSTRAINT [PK__TransferTables] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
